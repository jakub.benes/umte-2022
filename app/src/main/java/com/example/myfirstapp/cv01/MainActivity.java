package com.example.myfirstapp.cv01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.myfirstapp.R;

/**
 * Přechod mezi aktivitami.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openActivity(View view) {
        EditText editText = findViewById(R.id.editTextWelcomeText);

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("message", editText.getText().toString());
        startActivity(intent);
    }
}