package com.example.myfirstapp.cv01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.myfirstapp.R;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String welcomeText = intent.getStringExtra("message");

        TextView tvWelcomeText = findViewById(R.id.welcomeText);
        tvWelcomeText.setText(welcomeText);
    }
}