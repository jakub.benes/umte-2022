package com.example.myfirstapp.cv05;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private final int PERMISSION_REQUEST_CAMERA = 0;
    private final int PERMISSION_REQUEST_LOCATION = 1;
    private final int REQUEST_IMAGE_CAPTURE = 0;

    ImageView imagePreview;
    TextView tvCoordinates;

    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        findViewById(R.id.btnOpenCamera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCamera();
            }
        });

        findViewById(R.id.btnGetCoordinates).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCoordinates();
            }
        });

        imagePreview = findViewById(R.id.imagePreview);
        tvCoordinates = findViewById(R.id.tvCoordinates);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
            return;

        if (requestCode == PERMISSION_REQUEST_CAMERA) {
            Toast.makeText(getApplicationContext(), "Práva povolena", Toast.LENGTH_LONG).show();
            startCamera();
        } else if (requestCode == PERMISSION_REQUEST_LOCATION) {
            Toast.makeText(getApplicationContext(), "Location", Toast.LENGTH_LONG).show();
            getCoordinates();
        } else
            Toast.makeText(getApplicationContext(), "Práva zamítnuta", Toast.LENGTH_LONG).show();

    }

    private void requestCamera() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            // Práva máme, není potřeba o ně žádat
            Toast.makeText(getApplicationContext(), "Práva již přidělena, není nutné znovu žádat", Toast.LENGTH_LONG).show();
            startCamera();
        } else {
            // Požádáme o práva
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
        }
    }

    private void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != REQUEST_IMAGE_CAPTURE || resultCode != RESULT_OK || data == null)
            return;

        Bundle extras = data.getExtras();
        Bitmap image = (Bitmap) extras.get("data");
        imagePreview.setImageBitmap(image);
    }

    private void requestCoordinates() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCoordinates();
        } else {
            // Požádáme o práva
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
        }
    }

    @SuppressLint("MissingPermission")
    private void getCoordinates() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null)
                            tvCoordinates.setText("lat: " + location.getLatitude() + ", Lon: " + location.getLongitude());
                    }
                });
    }
}