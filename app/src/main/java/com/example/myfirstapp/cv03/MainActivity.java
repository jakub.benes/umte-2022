package com.example.myfirstapp.cv03;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfirstapp.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Ukládání do souboru.
 */
public class MainActivity extends AppCompatActivity {

    private Button btnSave, btnTakePhoto;
    private EditText txtTextToSave;

    private final String FILE_NAME = "result.txt";
    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        try {
            String textFromFile = readFile();
            counter = Integer.parseInt(textFromFile);
            Toast.makeText(getApplicationContext(), "OnCreate: " + textFromFile, Toast.LENGTH_LONG).show();
        }
        catch (Exception ex){
            Toast.makeText(getApplicationContext(), "Chyba: " + ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Button clicked!", Toast.LENGTH_LONG).show();
                saveToFile(txtTextToSave.getText().toString());
            }
        });

        btnTakePhoto = (Button) findViewById(R.id.btnTakePhoto);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        txtTextToSave = (EditText) findViewById(R.id.txtTextToSave);
    }

    @Override
    protected void onResume() {
        super.onResume();
        saveToFile(String.valueOf(counter++));
    }

    private void saveToFile(String text) {
        try (FileOutputStream fos = getApplicationContext().openFileOutput(FILE_NAME, getApplicationContext().MODE_PRIVATE)) {
            fos.write(text.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String readFile() {
        StringBuilder stringBuilder = new StringBuilder();
        FileInputStream fis = null;
        try {
            fis = getApplicationContext().openFileInput(FILE_NAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        InputStreamReader inputStreamReader =
                new InputStreamReader(fis, StandardCharsets.UTF_8);
        try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
            String line = reader.readLine();
            while (line != null) {
                stringBuilder.append(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            // Error occurred when opening raw file for reading.
        } finally {
            String contents = stringBuilder.toString();
        }

        return stringBuilder.toString();
    }
}